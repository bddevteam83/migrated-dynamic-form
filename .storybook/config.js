import 'hammerjs';

import "../node_modules/@angular/material/prebuilt-themes/indigo-pink.css";
import "../node_modules/@mdi/font/css/materialdesignicons.css";

import { configure } from '@storybook/angular';

// automatically import all files ending in *.stories.ts
configure(require.context('../projects/bd-innovations/dynamic-form/src/stories', true, /\.stories\.ts$/), module);
