import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DynamicFormArrayComponent} from './dynamic-form-array.component';
import {Component, Input} from '@angular/core';
import {GroupQuestion} from '../models/questions/reactive/group.question';
import {FormArray, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {ArrayQuestion} from '../models/questions/reactive/array.question';
import {DynamicFormGroupMockComponent} from '../dynamic-form-group/dynamic-form-group.component.spec';
import {DynamicFormControlMockComponent} from '../dynamic-form-control/dynamic-form-control.component.spec';

describe('DynamicFormArrayComponent', () => {
  let component: DynamicFormArrayComponent;
  let fixture: ComponentFixture<DynamicFormArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DynamicFormControlMockComponent,
        DynamicFormGroupMockComponent,

        DynamicFormArrayComponent
      ],
      imports: [
        ReactiveFormsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormArrayComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'bd-dynamic-form-array',
  template: '<p>dynamic-form-array works!</p>',
  styles: []
})
export class DynamicFormControlArrayMockComponent {
  @Input() question: ArrayQuestion;
  @Input() array: FormArray;
  @Input() arrayName: string;
}
