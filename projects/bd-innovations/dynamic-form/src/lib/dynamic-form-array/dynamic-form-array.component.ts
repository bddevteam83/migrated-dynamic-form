import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {instanceOfGroupQuestion} from '../models/questions/reactive/group.question';
import {FormArray, FormGroup} from '@angular/forms';
import {ArrayQuestion, instanceOfArrayQuestion} from '../models/questions/reactive/array.question';
import {instanceOfControlQuestion} from '../models/questions/reactive/control.question';

@Component({
  selector: 'bd-dynamic-form-array',
  templateUrl: './dynamic-form-array.component.html',
  styleUrls: ['./dynamic-form-array.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormArrayComponent implements OnInit {

  @Input() question: ArrayQuestion;
  @Input() array: FormArray;
  @Input() arrayName: string;
  @Input() group: FormGroup;

  readonly instanceOfControlQuestion = instanceOfControlQuestion;
  readonly instanceOfGroupQuestion = instanceOfGroupQuestion;
  readonly instanceOfArrayQuestion = instanceOfArrayQuestion;

  constructor() {
  }

  ngOnInit() {
  }

}
