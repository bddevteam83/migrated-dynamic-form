import {TestBed} from '@angular/core/testing';
import {DynamicFormService} from './dynamic-form.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import * as faker from 'faker';
import {AsyncValidatorConfig, AsyncValidatorsConfig} from './models/configs/async-validator.config';
import {ControlQuestion} from './models/questions/reactive/control.question';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValidatorsConfig} from './models/configs/validators.config';
import {GroupQuestion} from './models/questions/reactive/group.question';
import {ArrayQuestion} from './models/questions/reactive/array.question';

describe('DynamicFormService', () => {
  let service: DynamicFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DynamicFormService]
    });

    service = TestBed.get(DynamicFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('(control generators test)', () => {
    const validatorsConfigMock: ValidatorsConfig = null;
    const asyncValidatorsConfigMock: AsyncValidatorsConfig = null;

    describe('#generateFormControl(question: ControlQuestion)', () => {
      it('should return new FormControl based on passed args', () => {
        const questionMock: ControlQuestion = {
          discriminator: 'control',
          control: 'none',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          initValue: faker.random.word()
        };

        const res: any = (service as any).generateFormControl(questionMock);

        expect(res instanceof FormControl).toEqual(true);
        expect(res.value).toEqual(questionMock.initValue);
        expect(res.validator).toEqual(questionMock.validators);
        expect(res.asyncValidator).toEqual(questionMock.asyncValidators);
      });

      it('should throw an error if passed args are wrong', () => {
        const questionWithWrongDiscriminatorMock: any = {
          discriminator: faker.random.word(),
        };
        const questionWithoutDiscriminatorMock: any = {};

        expect(() => (service as any).generateFormControl(questionWithWrongDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithWrongDiscriminatorMock));
        expect(() => (service as any).generateFormControl(questionWithoutDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithoutDiscriminatorMock));
      });
    });

    describe('#generateFormGroup(question: GroupQuestion)', () => {
      it('should return new FormGroup based on passed args', () => {
        const initValues: string[] = Array.apply(null, {length: 4})
          .map(() => faker.random.word());
        const arrayQuestionMock: ArrayQuestion = {
          discriminator: 'array',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          controls: [
            {
              discriminator: 'control',
              validators: validatorsConfigMock,
              asyncValidators: asyncValidatorsConfigMock,
              control: 'none',
              initValue: initValues[0]
            },
            {
              discriminator: 'control',
              validators: validatorsConfigMock,
              asyncValidators: asyncValidatorsConfigMock,
              control: 'none',
              initValue: initValues[1]
            }
          ]
        };
        const controlQuestionMock: ControlQuestion = {
          discriminator: 'control',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          control: 'none',
          initValue: initValues[2]
        };
        const groupQuestionMock: GroupQuestion = {
          discriminator: 'group',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          controls: {
            nestedControl: {
              discriminator: 'control',
              validators: validatorsConfigMock,
              asyncValidators: asyncValidatorsConfigMock,
              control: 'none',
              initValue: initValues[3]
            }
          }
        };

        const questionMock: GroupQuestion = {
          discriminator: 'group',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          controls: {
            array: arrayQuestionMock,
            control: controlQuestionMock,
            group: groupQuestionMock
          }
        };

        const res = (service as any).generateFormGroup(questionMock);

        expect(res instanceof FormGroup).toBe(true);
        expect(res.validator).toEqual(questionMock.validators);
        expect(res.asyncValidator).toEqual(questionMock.asyncValidators);

        expect(res.get('array') instanceof FormArray).toBe(true);
        expect(res.get('array').validator).toEqual(arrayQuestionMock.validators);
        expect(res.get('array').asyncValidator).toEqual(arrayQuestionMock.asyncValidators);

        expect(res.get('array.0') instanceof FormControl).toBe(true);
        expect(res.get('array.0').value).toBe(initValues[0]);
        expect(res.get('array.0').validator).toEqual(arrayQuestionMock.controls[0].validators);
        expect(res.get('array.0').asyncValidator).toEqual(arrayQuestionMock.controls[0].asyncValidators);

        expect(res.get('array.1') instanceof FormControl).toBe(true);
        expect(res.get('array.1').value).toBe(initValues[1]);
        expect(res.get('array.1').validator).toEqual(arrayQuestionMock.controls[1].validators);
        expect(res.get('array.1').asyncValidator).toEqual(arrayQuestionMock.controls[1].asyncValidators);

        expect(res.get('control') instanceof FormControl).toBe(true);
        expect(res.get('control').value).toBe(initValues[2]);
        expect(res.get('control').validator).toEqual(controlQuestionMock.validators);
        expect(res.get('control').asyncValidator).toEqual(controlQuestionMock.asyncValidators);

        expect(res.get('group') instanceof FormGroup).toBe(true);
        expect(res.get('group').validator).toEqual(groupQuestionMock.validators);
        expect(res.get('group').asyncValidator).toEqual(groupQuestionMock.asyncValidators);

        expect(res.get('group.nestedControl') instanceof FormControl).toBe(true);
        expect(res.get('group.nestedControl').value).toBe(initValues[3]);
        expect(res.get('group.nestedControl').validator).toEqual(groupQuestionMock.controls.nestedControl.validators);
        expect(res.get('group.nestedControl').asyncValidator).toEqual(groupQuestionMock.controls.nestedControl.asyncValidators);
      });

      it('should throw an error if passed groupQuestion is wrong', () => {
        const questionWithWrongDiscriminatorMock: any = {
          discriminator: faker.random.word(),
        };
        const questionWithoutDiscriminatorMock: any = {};

        expect(() => (service as any).generateFormGroup(questionWithWrongDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithWrongDiscriminatorMock));
        expect(() => (service as any).generateFormGroup(questionWithoutDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithoutDiscriminatorMock));
      });

      it('should throw an error if passed groupQuestion controls are wrong', () => {
        const questionWithWrongNestedDiscriminatorMock: any = {
          discriminator: 'group',
          controls: {
            control: {
              discriminator: faker.random.word()
            }
          }
        };
        const questionWithoutNestedDiscriminatorMock: any = {
          discriminator: 'group',
          controls: {
            control: {}
          }
        };

        expect(() => (service as any).generateFormGroup(questionWithWrongNestedDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithWrongNestedDiscriminatorMock.controls.control));
        expect(() => (service as any).generateFormGroup(questionWithoutNestedDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithoutNestedDiscriminatorMock.controls.control));
      });
    });

    describe('#generateFormArray(question: ArrayQuestion)', () => {
      it('should return new FormArray based on passed args', () => {
        const initValues: string[] = Array.apply(null, {length: 4})
          .map(() => faker.random.word());
        const controlQuestionMock: ControlQuestion = {
          discriminator: 'control',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          control: 'none',
          initValue: initValues[2]
        };
        const groupQuestionMock: GroupQuestion = {
          discriminator: 'group',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          controls: {
            nestedControl: {
              discriminator: 'control',
              validators: validatorsConfigMock,
              asyncValidators: asyncValidatorsConfigMock,
              control: 'none',
              initValue: initValues[3]
            }
          }
        };
        const arrayQuestionMock: ArrayQuestion = {
          discriminator: 'array',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          controls: [
            {
              discriminator: 'control',
              validators: validatorsConfigMock,
              asyncValidators: asyncValidatorsConfigMock,
              control: 'none',
              initValue: initValues[0]
            },
            {
              discriminator: 'control',
              validators: validatorsConfigMock,
              asyncValidators: asyncValidatorsConfigMock,
              control: 'none',
              initValue: initValues[1]
            }
          ]
        };

        const questionMock: ArrayQuestion = {
          discriminator: 'array',
          validators: validatorsConfigMock,
          asyncValidators: asyncValidatorsConfigMock,
          controls: [
            controlQuestionMock,
            groupQuestionMock,
            arrayQuestionMock
          ]
        };

        const res = (service as any).generateFormArray(questionMock);

        expect(res instanceof FormArray).toBe(true);
        expect(res.validator).toEqual(questionMock.validators);
        expect(res.asyncValidator).toEqual(questionMock.asyncValidators);

        expect(res.get('0') instanceof FormControl).toBe(true);
        expect(res.get('0').value).toBe(initValues[2]);
        expect(res.get('0').validator).toEqual(controlQuestionMock.validators);
        expect(res.get('0').asyncValidator).toEqual(controlQuestionMock.asyncValidators);

        expect(res.get('1') instanceof FormGroup).toBe(true);
        expect(res.get('1').validator).toEqual(groupQuestionMock.validators);
        expect(res.get('1').asyncValidator).toEqual(groupQuestionMock.asyncValidators);

        expect(res.get('1.nestedControl') instanceof FormControl).toBe(true);
        expect(res.get('1.nestedControl').value).toBe(initValues[3]);
        expect(res.get('1.nestedControl').validator).toEqual(groupQuestionMock.controls.nestedControl.validators);
        expect(res.get('1.nestedControl').asyncValidator).toEqual(groupQuestionMock.controls.nestedControl.asyncValidators);

        expect(res.get('2') instanceof FormArray).toBe(true);
        expect(res.get('2').validator).toEqual(arrayQuestionMock.validators);
        expect(res.get('2').asyncValidator).toEqual(arrayQuestionMock.asyncValidators);

        expect(res.get('2.0') instanceof FormControl).toBe(true);
        expect(res.get('2.0').value).toBe(initValues[0]);
        expect(res.get('2.0').validator).toEqual(arrayQuestionMock.controls[0].validators);
        expect(res.get('2.0').asyncValidator).toEqual(arrayQuestionMock.controls[0].asyncValidators);

        expect(res.get('2.1') instanceof FormControl).toBe(true);
        expect(res.get('2.1').value).toBe(initValues[1]);
        expect(res.get('2.1').validator).toEqual(arrayQuestionMock.controls[1].validators);
        expect(res.get('2.1').asyncValidator).toEqual(arrayQuestionMock.controls[1].asyncValidators);
      });

      it('should throw an error if passed arrayQuestion is wrong', () => {
        const questionWithWrongDiscriminatorMock: any = {
          discriminator: faker.random.word(),
        };
        const questionWithoutDiscriminatorMock: any = {};

        expect(() => (service as any).generateFormArray(questionWithWrongDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithWrongDiscriminatorMock));
        expect(() => (service as any).generateFormArray(questionWithoutDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithoutDiscriminatorMock));
      });

      it('should throw an error if passed arrayQuestion controls are wrong', () => {
        const questionWithWrongNestedDiscriminatorMock: any = {
          discriminator: 'array',
          controls: [
            {
              discriminator: faker.random.word()
            }
          ]
        };
        const questionWithoutNestedDiscriminatorMock: any = {
          discriminator: 'array',
          controls: [
            {}
          ]
        };

        expect(() => (service as any).generateFormArray(questionWithWrongNestedDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithWrongNestedDiscriminatorMock.controls[0]));
        expect(() => (service as any).generateFormArray(questionWithoutNestedDiscriminatorMock))
          .toThrowError('Unknown ReactiveQuestion type, can\'t build the form.\n'
            + JSON.stringify(questionWithoutNestedDiscriminatorMock.controls[0]));
      });
    });
  });

  describe('#generateValidatorFns(config: ValidatorsConfig)', () => {
    it('should return array of ValidatorFn based on passed args', () => {
      const mockArgs: ValidatorsConfig = {
        required: true,
        pattern: faker.random.word(),
        email: true,
        minLength: faker.random.number(),
        maxLength: faker.random.number(),
        min: faker.random.number()
      };

      const res = (service as any).generateValidatorFns(mockArgs);

      expect(res).toContain(Validators.required);
      expect(res.map(elem => elem.toString())).toContain(Validators.pattern(mockArgs.pattern).toString());
      expect(res).toContain(Validators.email);
      expect(res.map(elem => elem.toString())).toContain(Validators.minLength(mockArgs.minLength).toString());
      expect(res.map(elem => elem.toString())).toContain(Validators.maxLength(mockArgs.maxLength).toString());
      expect(res.map(elem => elem.toString())).toContain(Validators.min(mockArgs.min).toString());
    });
  });

  describe('#generateAsyncValidatorFns(config: AsyncValidatorsConfig)', () => {

  });

  describe('#requestAsyncValidation(config: AsyncValidatorConfig, value: any)', () => {
    let httpTestingController: HttpTestingController;
    const partialArgsMock: Partial<AsyncValidatorConfig> = {
      apiPath: faker.random.word(),
      errorKey: faker.random.word(),
      errorMessage: faker.random.word(),
    };
    const requestDataMock: string = faker.random.word();
    const responseDataMock: string = faker.random.word();

    beforeEach(() => {
      httpTestingController = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
      httpTestingController.verify();
    });

    it('should call http request according to passed args (method: GET)', done => {
      const argsMock: AsyncValidatorConfig = {...partialArgsMock, apiMethod: 'GET'} as AsyncValidatorConfig;

      (service as any).requestAsyncValidation(argsMock, requestDataMock)
        .subscribe(res => {
          expect(res).toEqual(responseDataMock);
          done();
        });

      httpTestingController.expectOne({
        url: `${argsMock.apiPath}/${requestDataMock}`,
        method: 'GET'
      }).flush(responseDataMock);
    });

    it('should call http request according to passed args (method: POST)', done => {
      const argsMock: AsyncValidatorConfig = {...partialArgsMock, apiMethod: 'POST'} as AsyncValidatorConfig;

      (service as any).requestAsyncValidation(argsMock, requestDataMock)
        .subscribe(res => {
          expect(res).toEqual(responseDataMock);
          done();
        });

      const request = httpTestingController.expectOne({
        url: argsMock.apiPath,
        method: 'POST'
      });

      expect(request.request.body).toEqual(requestDataMock);

      request.flush(responseDataMock);
    });

    it('should call http request according to passed args (method: PUT)', done => {
      const argsMock: AsyncValidatorConfig = {...partialArgsMock, apiMethod: 'PUT'} as AsyncValidatorConfig;

      (service as any).requestAsyncValidation(argsMock, requestDataMock)
        .subscribe(res => {
          expect(res).toEqual(responseDataMock);
          done();
        });

      const request = httpTestingController.expectOne({
        url: argsMock.apiPath,
        method: 'PUT'
      });

      expect(request.request.body).toEqual(requestDataMock);

      request.flush(responseDataMock);
    });

    it('should call http request according to passed args (method: DELETE)', done => {
      const argsMock: AsyncValidatorConfig = {...partialArgsMock, apiMethod: 'DELETE'} as AsyncValidatorConfig;

      (service as any).requestAsyncValidation(argsMock, requestDataMock)
        .subscribe(res => {
          expect(res).toEqual(responseDataMock);
          done();
        });

      httpTestingController.expectOne({
        url: `${argsMock.apiPath}/${requestDataMock}`,
        method: 'DELETE'
      }).flush(responseDataMock);
    });

    it('should throw an error if the wrong config.apiMethod was passed or wasn\'t passed at all', () => {
      const argsWithWrongApiMethodMock: any = {...partialArgsMock, apiMethod: faker.random.word()};
      const argsWithoutApiMethodMock: any = {...partialArgsMock};

      expect(() => (service as any).requestAsyncValidation(argsWithWrongApiMethodMock, requestDataMock))
        .toThrowError('Unknown AsyncValidatorConfig.apiMethod, can\'t request form validation.\n'
          + JSON.stringify(argsWithWrongApiMethodMock));
      expect(() => (service as any).requestAsyncValidation(argsWithoutApiMethodMock, requestDataMock))
        .toThrowError('Unknown AsyncValidatorConfig.apiMethod, can\'t request form validation.\n'
          + JSON.stringify(argsWithoutApiMethodMock));

      httpTestingController.expectNone(() => true, 'and don\'t call any http request');
    });
  });
});

export class DynamicFormMockService {
  generateForm: any = (
    () => {
    }
  );
}
