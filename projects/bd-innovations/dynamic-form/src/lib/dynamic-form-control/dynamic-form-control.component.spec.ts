import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DynamicFormControlComponent} from './dynamic-form-control.component';
import {Component, forwardRef, Input} from '@angular/core';
import {GroupQuestion} from '../models/questions/reactive/group.question';
import {FormControl, FormGroup, NG_VALUE_ACCESSOR, ReactiveFormsModule} from '@angular/forms';
import {DynamicFormGroupMockComponent} from '../dynamic-form-group/dynamic-form-group.component.spec';
import {AbstractControlComponent} from '@bd-innovations/component-collection';
import {By} from '@angular/platform-browser';
import * as faker from 'faker';

@Component({
  selector: 'bd-input',
  template: '<p>input works!</p>',
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputMockComponent),
    multi: true
  }]
})
class InputMockComponent extends AbstractControlComponent {
  @Input() type: 'text' | 'number' | 'date' | 'email' | 'password' | 'tel' | 'url' = 'text';
  @Input() patternType = 'default';
  @Input() suffix: string;
  @Input() prefix: string;
}

@Component({
  selector: 'bd-select',
  template: '<p>select works!</p>',
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectMockComponent),
    multi: true
  }]
})
class SelectMockComponent extends AbstractControlComponent {
  @Input() options: any[] = [];
  @Input() optionDisplayProp: string | string[];
  @Input() valueProp: string;
  @Input() multiple: boolean;
  @Input() nullable: boolean;
  @Input() compareWith: (o1: any, o2: any) => boolean = (o1: any, o2: any) => o1 === o2;
}

@Component({
  selector: 'bd-datepicker',
  template: '<p>datepicker works!</p>',
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DatepickerMockComponent),
    multi: true
  }]
})
class DatepickerMockComponent extends AbstractControlComponent {
  @Input() min: Date;
  @Input() max: Date;
}

describe('DynamicFormControlComponent', () => {
  let component: DynamicFormControlComponent;
  let fixture: ComponentFixture<DynamicFormControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DynamicFormControlMockComponent,
        DynamicFormGroupMockComponent,

        InputMockComponent,
        SelectMockComponent,
        DatepickerMockComponent,

        DynamicFormControlComponent
      ],
      imports: [
        ReactiveFormsModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormControlComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('(tests with template)', () => {
    it('should render form control according to passed inputs (TEXTBOX)', () => {
      component.question = {
        discriminator: 'control',
        control: {
          discriminator: 'textbox',
          type: 'text'
        }
      };
      component.control = new FormControl(null);

      fixture.detectChanges();
      const componentMock: InputMockComponent = fixture.debugElement.children[0].componentInstance;

      expect(fixture.debugElement.query(By.css('bd-input'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('bd-select'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('bd-datepicker'))).toBeFalsy();
      // TODO how to compare form controls?
      expect(componentMock.control).not.toBeUndefined();
    });

    // TODO test here also object dropdown
    // TODO test passing compareWith to form control
    it('should render form control according to passed inputs (DROPDOWN)', () => {
      component.question = {
        discriminator: 'control',
        control: {
          discriminator: 'dropdown',
          options: Array.apply(null, {length: 5})
            .map(() => faker.random.word())
        }
      };
      component.control = new FormControl(null);

      fixture.detectChanges();
      const componentMock: InputMockComponent = fixture.debugElement.children[0].componentInstance;

      expect(fixture.debugElement.query(By.css('bd-input'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('bd-select'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('bd-datepicker'))).toBeFalsy();
      // TODO how to compare form controls?
      expect(componentMock.control).not.toBeUndefined();
    });

    it('should render form control according to passed inputs (DATEPICKER)', () => {
      component.question = {
        discriminator: 'control',
        control: {
          discriminator: 'datepicker',
          type: 'date'
        }
      };
      component.control = new FormControl(null);

      fixture.detectChanges();
      const componentMock: InputMockComponent = fixture.debugElement.children[0].componentInstance;

      expect(fixture.debugElement.query(By.css('bd-input'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('bd-select'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('bd-datepicker'))).toBeTruthy();
      // TODO how to compare form controls?
      expect(componentMock.control).not.toBeUndefined();
    });
  });
});

@Component({
  selector: 'bd-dynamic-form-control',
  template: '<p>dynamic-form-control works!</p>',
  styles: []
})
export class DynamicFormControlMockComponent {
  @Input() question: GroupQuestion;
  @Input() control: FormGroup;
}
