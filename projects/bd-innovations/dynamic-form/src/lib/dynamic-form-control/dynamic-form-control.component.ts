import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ControlQuestion} from '../models/questions/reactive/control.question';
import {DropdownQuestion} from '../models/questions/template/dropdown.question';

@Component({
  selector: 'bd-dynamic-form-control',
  templateUrl: './dynamic-form-control.component.html',
  styleUrls: ['./dynamic-form-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormControlComponent implements OnInit {

  @Input() question: ControlQuestion;
  @Input() control: FormControl;

  compareWithKey: (o1, o2) => boolean = (
    (o1, o2) => {
      const key = (this.question.control as unknown as DropdownQuestion<any>).keyToCompareWith;

      return key && o1 && o2 && o1[key] === o2[key];
    }
  );
  defaultCompareWith: (o1, o2) => boolean = (
    (o1, o2) => o1 === o2
  );

  constructor() {
  }

  ngOnInit() {
  }


}
