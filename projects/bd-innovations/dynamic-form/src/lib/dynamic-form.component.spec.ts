import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DynamicFormComponent} from './dynamic-form.component';
import {DynamicFormService} from './dynamic-form.service';
import {DynamicFormMockService} from './dynamic-form.service.spec';
import Spy = jasmine.Spy;
import * as faker from 'faker';
import {DynamicFormGroupMockComponent} from './dynamic-form-group/dynamic-form-group.component.spec';
import {By} from '@angular/platform-browser';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';

describe('DynamicFormComponent', () => {
  let component: DynamicFormComponent;
  let fixture: ComponentFixture<DynamicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DynamicFormGroupMockComponent,

        DynamicFormComponent
      ],
      imports: [
        ReactiveFormsModule
      ],
      providers: [
        {provide: DynamicFormService, useClass: DynamicFormMockService}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('(tests with template)', () => {
    let onInitSpy: Spy;

    beforeEach(() => {
      onInitSpy = spyOn(component, 'ngOnInit');
    });

    it('should render DynamicFormGroupComponent and pass there appropriate data if \'form\' is truthy', () => {
      component.form = new FormGroup({});
      component.metadata = faker.random.word() as any;

      fixture.detectChanges();
      expect(onInitSpy.calls.count()).toBe(1);
      const componentMock: DynamicFormGroupMockComponent = fixture.debugElement.children[0].componentInstance;

      expect(fixture.debugElement.query(By.css('bd-dynamic-form-group'))).toBeTruthy();
      expect(componentMock.group).toEqual(component.form);
      expect(componentMock.question).toEqual(component.metadata);
    });

    it('should not render DynamicFormGroupComponent if \'form\' is falsy', () => {
      fixture.detectChanges();
      expect(onInitSpy.calls.count()).toBe(1);

      expect(fixture.debugElement.query(By.css('bd-dynamic-form-group'))).toBeFalsy();
    });
  });
});
