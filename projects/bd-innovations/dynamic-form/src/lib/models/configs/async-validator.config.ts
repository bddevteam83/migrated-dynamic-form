export interface AsyncValidatorConfig {
  apiPath: string;
  apiMethod: 'GET' | 'POST' | 'PUT' | 'DELETE';
  errorKey: string;
  errorMessage: string;
}

export type AsyncValidatorsConfig = AsyncValidatorConfig[];
