export interface ValidatorsConfig {
  required?: true;
  pattern?: string | RegExp;
  email?: true;
  minLength?: number;
  maxLength?: number;
  min?: number;
}
