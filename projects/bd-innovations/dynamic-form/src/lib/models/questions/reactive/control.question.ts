import {TextboxQuestion} from '../template/textbox.question';
import {DropdownQuestion} from '../template/dropdown.question';
import {BaseReactiveQuestion} from './base-reactive.question';
import {DatepickerQuestion} from '../template/datepicker.question';

export interface ControlQuestion<T = string> extends BaseReactiveQuestion<T> {
  discriminator: 'control';
  initValue?: T;
  control: TextboxQuestion | DropdownQuestion<T> | DatepickerQuestion | 'none';
}

export function instanceOfControlQuestion(object: any): object is ControlQuestion {
  return object.discriminator === 'control';
}
