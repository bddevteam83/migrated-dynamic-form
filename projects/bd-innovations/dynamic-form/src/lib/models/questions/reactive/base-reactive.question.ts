import {TypedInterface} from '../../utilities/typed-interface';
import {ValidatorsConfig} from '../../configs/validators.config';
import {AsyncValidatorsConfig} from '../../configs/async-validator.config';

export interface BaseReactiveQuestion<T> extends TypedInterface {
  validators?: ValidatorsConfig;
  asyncValidators?: AsyncValidatorsConfig;
}
