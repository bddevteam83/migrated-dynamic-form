import {ControlQuestion} from './control.question';
import {ArrayQuestion} from './array.question';
import {BaseReactiveQuestion} from './base-reactive.question';

export interface GroupQuestion<T = object> extends BaseReactiveQuestion<T> {
  discriminator: 'group';
  controls: {
    [key: string]: ControlQuestion<any> | GroupQuestion<any> | ArrayQuestion<any>;
  };
}

export function instanceOfGroupQuestion(object: any): object is GroupQuestion {
  return object.discriminator === 'group';
}
