import {ControlQuestion} from './control.question';
import {GroupQuestion} from './group.question';
import {BaseReactiveQuestion} from './base-reactive.question';

export interface ArrayQuestion<T = any[]> extends BaseReactiveQuestion<T> {
  discriminator: 'array';
  controls: Array<ControlQuestion<any> | GroupQuestion<any> | ArrayQuestion>;
}

export function instanceOfArrayQuestion(object: any): object is ArrayQuestion {
  return object.discriminator === 'array';
}
