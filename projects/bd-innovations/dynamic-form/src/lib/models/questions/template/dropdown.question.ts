import {BaseTemplateQuestion} from './base-template.question';
import {TextboxQuestion} from './textbox.question';

export interface DropdownQuestion<T> extends BaseTemplateQuestion<T> {
  discriminator: 'dropdown';
  options: T[];
  keyToCompareWith?: keyof T;
  nullable?: true;
  multi?: true;
  optionDisplayProp?: string;
  valueProp?: string;
}

export function instanceOfDropdownQuestion(object: any): object is TextboxQuestion {
  return object.discriminator === 'dropdown';
}
