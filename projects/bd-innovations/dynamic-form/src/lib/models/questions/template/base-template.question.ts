import {TypedInterface} from '../../utilities/typed-interface';

export interface BaseTemplateQuestion<T> extends TypedInterface {
  label?: string;
  placeholder?: string;
}
