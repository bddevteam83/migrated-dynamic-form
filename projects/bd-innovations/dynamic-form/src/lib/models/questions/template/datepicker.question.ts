import {BaseTemplateQuestion} from './base-template.question';

export interface DatepickerQuestion extends BaseTemplateQuestion<string> {
  discriminator: 'datepicker';
  type: DatepickerType;
  min?: Date;
  max?: Date;
}

export type DatepickerType = 'date' | 'datetime-local';

export function instanceOfDatepickerQuestion(object: any): object is DatepickerQuestion {
  return object.discriminator === 'datepicker';
}
