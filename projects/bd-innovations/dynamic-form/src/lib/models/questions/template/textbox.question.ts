import {BaseTemplateQuestion} from './base-template.question';

export interface TextboxQuestion extends BaseTemplateQuestion<string> {
  discriminator: 'textbox';
  type: TextboxType;
}

export type TextboxType = 'text' | 'email' | 'number' | 'password';

export function instanceOfTextboxQuestion(object: any): object is TextboxQuestion {
  return object.discriminator === 'textbox';
}
