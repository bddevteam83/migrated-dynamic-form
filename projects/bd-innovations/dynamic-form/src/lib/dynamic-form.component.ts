import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {GroupQuestion} from './models/questions/reactive/group.question';

@Component({
  selector: 'bd-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() metadata: GroupQuestion;

  constructor() {
  }

  ngOnInit() {
  }

}
