import {NgModule} from '@angular/core';
import {DynamicFormComponent} from './dynamic-form.component';
import {DynamicFormService} from './dynamic-form.service';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {OrderedKeyValuePipeModule} from '@bd-innovations/pipe-collection';
import {DynamicFormControlComponent} from './dynamic-form-control/dynamic-form-control.component';
import {DynamicFormGroupComponent} from './dynamic-form-group/dynamic-form-group.component';
import {DynamicFormArrayComponent} from './dynamic-form-array/dynamic-form-array.component';
import {DatepickerComponentModule, InputComponentModule, SelectComponentModule} from '@bd-innovations/component-collection';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormControlComponent,
    DynamicFormGroupComponent,
    DynamicFormArrayComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    TranslateModule,

    InputComponentModule,
    SelectComponentModule,
    OrderedKeyValuePipeModule,
    DatepickerComponentModule,
  ],
  providers: [DynamicFormService],
  exports: [DynamicFormComponent]
})
export class DynamicFormModule {
}
