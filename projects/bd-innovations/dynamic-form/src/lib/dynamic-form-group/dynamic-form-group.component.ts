import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {GroupQuestion} from '../models/questions/reactive/group.question';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'bd-dynamic-form-group',
  templateUrl: './dynamic-form-group.component.html',
  styleUrls: ['./dynamic-form-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormGroupComponent implements OnInit {

  @Input() question: GroupQuestion;
  @Input() group: FormGroup;


  constructor() {
  }

  ngOnInit() {
  }

}
