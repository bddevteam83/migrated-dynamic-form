import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DynamicFormGroupComponent} from './dynamic-form-group.component';
import {Component, Input, Pipe} from '@angular/core';
import {GroupQuestion} from '../models/questions/reactive/group.question';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {DynamicFormControlArrayMockComponent} from '../dynamic-form-array/dynamic-form-array.component.spec';
import {DynamicFormControlMockComponent} from '../dynamic-form-control/dynamic-form-control.component.spec';
import {OrderedKeyValuePipeModule} from '@bd-innovations/pipe-collection';

describe('DynamicFormGroupComponent', () => {
  let component: DynamicFormGroupComponent;
  let fixture: ComponentFixture<DynamicFormGroupComponent>;

  // TODO mock OrderedKeyValuePipe
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DynamicFormControlMockComponent,
        DynamicFormControlArrayMockComponent,

        DynamicFormGroupComponent
      ],
      imports: [
        ReactiveFormsModule,

        OrderedKeyValuePipeModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormGroupComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('(tests with template)', () => {
    beforeEach(() => {
      component.group = new FormGroup({});
      component.question = {} as any;

      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});

@Component({
  selector: 'bd-dynamic-form-group',
  template: '<p>dynamic-form-group works!</p>',
  styles: []
})
export class DynamicFormGroupMockComponent {
  @Input() question: GroupQuestion;
  @Input() group: FormGroup;
}
