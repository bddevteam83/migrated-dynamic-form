import {moduleMetadata, storiesOf} from '@storybook/angular';
import {DynamicFormComponent} from '../lib/dynamic-form.component';
import {CommonModule} from '@angular/common';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {DynamicFormControlComponent} from '../lib/dynamic-form-control/dynamic-form-control.component';
import {DynamicFormGroupComponent} from '../lib/dynamic-form-group/dynamic-form-group.component';
import {DynamicFormArrayComponent} from '../lib/dynamic-form-array/dynamic-form-array.component';
import {OrderedKeyValuePipeModule} from '@bd-innovations/pipe-collection';
import {DynamicFormService} from '../lib/dynamic-form.service';
import {HttpClientModule} from '@angular/common/http';
import {GroupQuestion} from '../lib/models/questions/reactive/group.question';
import {DatepickerComponentModule, InputComponentModule, SelectComponentModule} from '@bd-innovations/component-collection';
import {TranslateModule} from '@ngx-translate/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const props: { metadata: GroupQuestion, data: any, form: FormGroup } = {
  metadata: {
    discriminator: 'group',
    controls: {
      control: {
        discriminator: 'control',
        validators: {
          required: true
        },
        control: {
          discriminator: 'textbox',
          type: 'text',
          placeholder: 'Some Placeholder'
        },
      },
      anotherControl: {
        discriminator: 'control',
        validators: {
          required: true
        },
        initValue: {field: 'one'},
        control: {
          discriminator: 'dropdown',
          options: [{field: 'one'}, {field: 'two'}, {field: 'three'}],
          placeholder: 'Select something',
          optionDisplayProp: 'field',
          keyToCompareWith: 'field',
          multi: true
        }
      },
      oneMore: {
        discriminator: 'control',
        validators: {
          required: true
        },
        control: {
          discriminator: 'dropdown',
          options: ['four', 'five', 'six'],
          placeholder: 'simple select'
        }
      },
      datepicker: {
        discriminator: 'control',
        control: {
          discriminator: 'datepicker',
          type: 'date',
          placeholder: 'datepicker'
        }
      },
      array: {
        discriminator: 'array',
        controls: [
          {
            discriminator: 'control',
            control: 'none',
          },
          {
            discriminator: 'control',
            control: 'none',
          }
        ]
      }
    }
  },
  data: {
    control: 'control > value',
    array: [
      'array > 0 > value',
      'array > 1 > value'
    ]
  },
  form: new FormGroup({
    control: new FormControl(null, [Validators.required]),
    anotherControl: new FormControl({field: 'one'}, [Validators.required]),
    oneMore: new FormControl('four', [Validators.required]),
    datepicker: new FormControl(new Date()),
    array: new FormArray([
      new FormControl(null),
      new FormControl(null)
    ])
  })
};

storiesOf('Dynamic form', module)
  .addDecorator(moduleMetadata({
    declarations: [
      DynamicFormComponent,
      DynamicFormControlComponent,
      DynamicFormGroupComponent,
      DynamicFormArrayComponent
    ],
    imports: [
      CommonModule,
      ReactiveFormsModule,

      TranslateModule.forRoot(),

      BrowserAnimationsModule,
      HttpClientModule,

      InputComponentModule,
      SelectComponentModule,
      DatepickerComponentModule,
      OrderedKeyValuePipeModule,
    ],
    providers: [DynamicFormService],
  }))
  .add('regular', () => ({
    template: `<div class="container">
                <bd-dynamic-form [metadata]="metadata" [data]="data" [form]="form"></bd-dynamic-form>
               </div>`,
    styles: ['.container { width: 300px; }'],
    props
  }));
